import tensorflow_hub as hub
import os


class Model:

    def __init__(self, model=''):
        self.model = model
        self.models_dir = 'models'
        self.not_models = ['.DS_Store']
        self.models = list(os.listdir(f"./{self.models_dir}"))

        self.models = list(filter(lambda f: f not in self.not_models, self.models))

    def load(self):
        if self.model in self.models:
            self.embed = hub.load(
                f"{os.getcwd()}/{self.models_dir}/{self.model}")
            return self
        else:
            raise 'Model not found!'
