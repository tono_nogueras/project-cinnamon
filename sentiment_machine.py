import pandas as pd
from google.cloud import language_v1


class SentimentMachine:
    def __init__(self, text=None):
        """
        Analyzing Sentiment in a String

        Args:
        text_content The text content to analyze
        """
        self.text = text
        self.client_ = language_v1.LanguageServiceClient()
        self.type = language_v1.Document.Type.PLAIN_TEXT  # Available types: PLAIN_TEXT, HTML
        # Optional. If not specified, the language is automatically detected.
        # For list of supported languages:
        # https://cloud.google.com/natural-language/docs/languages
        self.language = "en"
        self.encoding_type = language_v1.EncodingType.UTF8  # Available values: NONE, UTF8, UTF16, UTF32
        self.sentiment = None

        if isinstance(self.text, str):
            self.gather_sentiment(self.text)

    def gather_sentiment(self, text_content=None):
        """
        Gather Sentiment in a String

        Args:
        text_content The text content to analyze in the cloud
        """
        text_handler = self.text if text_content == None else text_content
        document = {"content": text_handler, "type_": self.type, "language": self.language}

        response = self.client_.analyze_sentiment(request={'document': document, 'encoding_type': self.encoding_type})

        self.sentiment = response
        return self

    def analyze_sentiment(self, dataframe=False, provide_doc_scores=True):
        """
        Analyze gathered sentiment

        Args:
        dataframe display analysis as dataframe. Does not include document metrics.
        provide_doc_scores Appends overall document scores to first row of dataframe if checked. Default is True
        """
        try:
            doc_sent_score = self.sentiment.document_sentiment.score
            doc_sent_mag = self.sentiment.document_sentiment.magnitude

            if dataframe:
                data = {
                    "content": [],
                    "sent_score": [],
                    "sent_mag": [],
                }
                if provide_doc_scores:
                    data["content"].append(self.text)
                    data["sent_score"].append(doc_sent_score)
                    data["sent_mag"].append(doc_sent_mag)

                for sentence in self.sentiment.sentences:
                    data["content"].append(sentence.text.content)
                    data["sent_score"].append(sentence.sentiment.score)
                    data["sent_mag"].append(sentence.sentiment.magnitude)

                df = pd.DataFrame(data)

                return df

            else:
                # Get overall sentiment of the input document
                print(u"Document sentiment score: {}".format(doc_sent_score))
                print(u"Document sentiment magnitude: {}\n".format(doc_sent_mag))

                # Get sentiment for all sentences in the document
                for sentence in self.sentiment.sentences:
                    print(u"Sentence text: \"{}\"".format(sentence.text.content))
                    print(u"Sentence sentiment score: {}".format(sentence.sentiment.score))
                    print(u"Sentence sentiment magnitude: {}\n".format(sentence.sentiment.magnitude))

                # Get the language of the text, which will be the same as
                # the language specified in the request or, if not specified,
                # the automatically-detected language.
                print(u"Language of the text: {}".format(self.sentiment.language))
        except Exception as e:
            print(e)
