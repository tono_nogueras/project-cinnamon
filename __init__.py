from model import Model
from synonym_matcher import SynonymMatcher

if __name__ == "__main__":
    model = Model('nnlm-en-dim50_2').load()
    syn = SynonymMatcher(model, dataframe=True)
    print(syn.match_analysis("hi", ["sup", "eat", "dumb"]))
