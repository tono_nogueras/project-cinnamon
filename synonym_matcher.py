import pandas as pd
import numpy as np

dev_scale = 1


class SynonymMatcher:

    def __init__(self, model_embed, words=None, dataframe=False):
        self.model_embed = model_embed
        self.dataframe = dataframe
        self.words = words

    @staticmethod
    def trim_scalars(df, i):
        max_v = (df.mean() - (df.std() * dev_scale)).to_numpy()[0]
        try:
            return df[df.scalars < max_v].head(3).words.to_numpy()[i]
        except IndexError:
            pass
        return ""

    # FIXME - Optimize the matching out from inside loop
    def apply_trim(self, i, words=None):
        return lambda e: self.trim_scalars(self.match_analysis(e, words), i)

    def categorize(self, df, words=None):
        df['first_word'] = df['content'].apply(self.apply_trim(0, words))
        df['second_word'] = df['content'].apply(self.apply_trim(1, words))
        df['third_word'] = df['content'].apply(self.apply_trim(2, words))
        return df

    def vector(self, first_word, second_word):
        embeddings = self.model_embed.embed([first_word, second_word])
        result = (embeddings[0] - embeddings[1]).numpy()
        
        return np.linalg.norm(result)

    def match_analysis(self, search_key, words=None):
        if not self.dataframe:
            print(f"\nbase word: {search_key}\n")

        if not words:
            words = self.words

        schema = {
            "words": [],
            "scalars": []
        }

        for word in words:
            scalar = self.vector(search_key, word)
            if self.dataframe:
                schema["words"].append(word)
                schema["scalars"].append(scalar)
            else:
                print(f"{word}: {scalar}")

        if self.dataframe:
            return pd.DataFrame(schema).sort_values('scalars', ascending=True)
