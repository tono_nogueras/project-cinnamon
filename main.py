from pprint import pprint

import pandas as pd
from sentiment_machine import SentimentMachine
from synonym_matcher import SynonymMatcher
from model import Model


def color_cols(x):
    red = 'color: red'
    green = 'color: green'
    # compare columns
    pos = x.sent_score > 0
    neg = x.sent_score < 0
    # DataFrame with same index and columns names as original filled empty strings
    df1 = pd.DataFrame('', index=x.index, columns=x.columns)
    # modify values of df1 column by boolean mask
    df1.loc[neg, 'sent_score'] = red
    df1.loc[pos, 'sent_score'] = green
    df1.loc[neg, 'content'] = red
    df1.loc[pos, 'content'] = green
    return df1


def present_df(df):
    new_df = df.copy()
    dropped_cols = ['first_word', 'second_word', 'third_word', 'sent_mag']
    words_column = (new_df.first_word + ', ' + new_df.second_word + ', ' + new_df.third_word).apply(
        lambda x: x.rstrip(', '))
    new_df.drop(dropped_cols, axis=1, inplace=True)
    new_df['words'] = words_column
    table_styles = {'background': 'black', 'max-width': '500px'}

    return new_df.style. \
        set_properties(**table_styles) \
        .apply(color_cols, axis=None) \
        .hide_index()


if __name__ == '__main__':
    model = Model('nnlm-en-dim128').load()

    demo_words = ["value", "customer service", "check in", "booking",
                  "rewards", "schedule", "safety", "bathroom", "clean"]

    review = "Other than the occasional glitch changing flights, this app is great like the airline. Once, after the " \
             "only mid-trip flight cancellation I've experienced, as I looked at the long and growing line at the " \
             "service counter the app pinged and had already assigned me a seat on the next flight out. This is one " \
             "of the reasons I drive four hours to get to an airport Southwest serves. United is only an hour and a " \
             "half away, but the service just isn't the same. "
    df = SentimentMachine(review).analyze_sentiment(dataframe=True)

    syn = SynonymMatcher(model, words=demo_words, dataframe=True)

    categorized = syn.categorize(df)
    results = present_df(categorized)
    pprint(results)


